# Toggle

For an overview of the features we plan to keep from Tweaks and move to Settings, see FEATURES.md.

## TODO

- [x] Convert the mockups to an AdwApplicationWindow
- [ ] Implement the logic for reading the tweaks
  - [ ] Button layout
  - [ ] Fonts
  - [ ] Headerbar actions
- [ ] Split the mockups into several files for each menu, and implement the TS bits
- [x] Make an icon
- [x] Add a debug info function
- [x] Add Blueprint support to `data/ui`

## Preview

Appearance | Desktop
---|---
![A mockup of the appearance tab, showing a list of appearance preferences](mockups/appearance.png) | ![A mockup of the desktop tab, showing a list of desktop environment preferences](mockups/desktop.png)

Desktop Expanded | Devices
---|---
![A mockup of the desktop tab, showing an expanded list of desktop environment preferences](mockups/desktop-expanded.png) | ![A mockup of the devices tab, showing a list of device preferences](mockups/devices.png)

Startup | Unsupported Options
---|---
![A mockup of the startup page, showing a list of startup apps](mockups/startup-apps.png)  | ![A mockup of the unsupported options page, asking for confirmation](mockups/unsupported-options.png)

## Motivation

GNOME Tweaks is barely maintained, many of the settings are broken in newer GNOME versions, and many of the settings have already been moved to GNOME Settings. Since many users still rely on a couple of settings that Tweaks provides, we need a solution.

## Goal

Our goal is to completely replace GNOME Tweaks with a modernized experience. This includes:

- Cleaned up settings, nothing that doesn't work / is already implemented in GNOME Settings
- A new UI, made with GTK4 and Libadwaita
- Shipped as a Flatpak, hosted on Flathub

Non-Flatpak builds of Toggle will **not** be supported.
